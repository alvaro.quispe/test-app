import IOption from "./option";

export default interface IQuestion {
  id: string,
  text: string,
  options: IOption[]
}