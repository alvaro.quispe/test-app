// Actions what support my reducer
import actions from './actions';
import IAction from '../../Models/action';
import fakeQuestions from './fake';
import IQuestion from '../../Models/question';
import IUser from '../../Models/user';

export const initialState = {
  questions: fakeQuestions,
  currentUser: {
    username: ''
  },
  error: undefined,
  view: { showForm: false }
};

function addUser(state: IState, user: IUser) {
  let newUser = state.currentUser;
  newUser={...state.currentUser, ...user};
  return { ...state, user: newUser };
}

function removeUser(state: IState, email: string) {
  state.currentUser={username:''};
  return { ...state, user: state.currentUser };
}

function updateQuestion(state: IState, question: IQuestion){
  const newQuestions = [...state.questions];
  newQuestions.map((item, index) => {
    return item=question
  })
}

export default function reducer(state: IState = initialState, action: IAction) {
  const { payload } = action;
  switch (action.type) {
    case actions.ADD_USER(payload).type:
      return addUser(state, payload);
    case actions.REMOVE_USER(payload).type:
      return removeUser(state, payload);
    case actions.UPDATE_QUESTION(payload).type:
      return updateQuestion(state, payload);
    default:
      return state;
  }
}

interface IState {
  questions: IQuestion[],
  currentUser: IUser,
  error: any,
}