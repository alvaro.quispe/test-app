import ActionCreator from '../../Helpers/ActionCreator';

const actionCreator = new ActionCreator('Home');

//TODO DEFINE Typo of actions.
export default {
  ADD_USER: (payload: any) => actionCreator.create('ADD_USER', payload),
  REMOVE_USER: (payload: any) => actionCreator.create('REMOVE_USER', payload),
  UPDATE_QUESTION: (payload: any) => actionCreator.create('UPDATE_QUESTION', payload),

  GET_QUESTIONS: (payload?: any) => actionCreator.create('GET_QUESTIONS', payload),
}