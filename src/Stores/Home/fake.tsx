import IQuestion from '../../Models/question';

const questions: IQuestion[] = [ { id: 'question1', text: 'question text', options: [ { id: 'a', text: 'b', selected: true } ] },
{ id: 'question1', text: 'question text', options: [ { id: 'a', text: 'b', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false } ] },
{ id: 'question2', text: 'question text2', options: [ { id: 'a', text: 'b', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false } ] },
{ id: 'question3', text: 'question text2', options: [ { id: 'a', text: 'b', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false } ] },
{ id: 'question4', text: 'question text2', options: [ { id: 'a', text: 'b', selected: true }, { id: 'b', text: 'b', selected: false }, { id: 'c', text: 'c', selected: false } ] },
] ;
export default questions;