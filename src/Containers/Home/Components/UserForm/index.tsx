import React, { MouseEvent, useState } from 'react';
import Form from '../../../../Components/Form';
import TextField from '@material-ui/core/TextField';
import './styles.css';
import IUser from '../../../../Models/user';

function UserForm({ onInvite } :IProps) {

  const [form, setForm] = useState({ username: '' });
  function handleLogin(event: MouseEvent<HTMLButtonElement>) {
    onInvite(form);
  }

  const actions = [
    {
    onClick: handleLogin,
    text: 'Invite',
    primary: true
  }];

  return (
    <section className="userFormCmpt">
      <Form actions={actions} isValid={false}>
        <TextField
          onChange={event => setForm({...form, username: event.target.value })}
          type="text"
          placeholder="username..."
          value={form.username}/>
      </Form>
    </section>
  )
}

interface IProps {
  onInvite: (user: IUser) => void,
}

export default UserForm;