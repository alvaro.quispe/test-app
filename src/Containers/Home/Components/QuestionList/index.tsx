import React from 'react';
import IQuestion from '../../../../Models/question';
import Question from '../../../../Components/Question';

function QuestionList({ questions }: IProps) {
  return (
    <section className="taskListCmpt">
      {questions.map(item => (
        <Question {...item} />
      ))}
    </section>
  );
}

interface IProps {
  questions: IQuestion[]
}

export default QuestionList;