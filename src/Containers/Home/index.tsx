import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import IUser from "../../Models/user";
import './styles.css';
import IOpetion from "../../Models/option";
import actions from "../../Stores/Home/actions";
import IQuestion from "../../Models/question";
import QuestionList from "./Components/QuestionList";

/* 
  1hacer dispatch de cualquier action
  2cycle of life component
*/

function Home({ user, questions, getQuestions, error }: IProps) {
  useEffect(() => {
    getQuestions();
  }, [getQuestions]);

  //TODO REMOVE THIS STATE SHOULD BE HANDLED IN THE STORE.
  const [currentUser, setCurrentUser] = useState();
  function handleClick(user: IUser) {
    setCurrentUser(currentUser);
  }

  return (
    <section className="homeContainer">
      <section className="questions">
        <QuestionList questions={questions} />
      </section>
    </section>
  );
}

//TODO: typo
function mapStateProps(state: any) {
  return {
    questions: state.questions,
    user: state.currentUser,
    error: state.error,
  };
}

function mapDispatchProps(dispatch: any) {
  return {
    getQuestions: function () {
      dispatch(actions.GET_QUESTIONS());
    }
  };
}

interface IProps {
  user: IUser,
  questions: IQuestion[],
  getQuestions: any,
  error: any,
}

export default connect(mapStateProps, mapDispatchProps)(Home);
