import React from 'react';
import IQuestion from '../../Models/question';
import './styles.css';
import IOption from '../../Models/option';
function Question({id, text, options }: IQuestion) {
  return (
    <section className="taskCmpt">
      <div>
        {text}
      </div>

      {options.map((item: IOption) => {
        return (
          <div>
            <p id={item.id}>{item.text}</p>
            <p id={item.id}>{item.selected}</p>
          </div>
        );
      })}
    </section>
  )
}

export default Question;